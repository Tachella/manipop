function [A,T0,B,map_delta,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points]=merge_move(Y,Y_ind,T,h,attack,decay,A,T0,B,lambda_S,max_dist,occupied_volume,prior_length,PPP,Npix,Nbin,gamma_strauss,NEIGH,points_with_neigh,neigh_sum,prior_pixels,eff_prior_length,sum_PPP,log_gamma_area_int,lambda_area_int,G,total_points,alpha,scale_Z,K,Nrow,Mergeable_pixels,beta_ms)
   
    map_delta = 0;
    %% pick a pixel
    merge_sum=sum(cellfun(@length,Mergeable_pixels).*(1:length(Mergeable_pixels))');
    p=rand*merge_sum;
    
    k=1;
    m=Mergeable_pixels{k};
    csum=length(m);
    while csum<=p
        k=k+1;
        m=Mergeable_pixels{k};
        csum=csum+length(m)*k;
    end
    pixel=m(randi([1,length(m)]));
    
    
    t0=T0{pixel};
    accepted = false;
    while ~accepted
        index=randi(length(t0));
        for j=1:length(t0)
            if j~=index && abs(t0(j)-t0(index))<=attack+decay
                accepted=true;
                index_2=j;
                if index_2<index
                    aux = index_2;
                    index_2=index;
                    index=aux;
                end
                break;
            end
        end
    end
    
    a = A{pixel};
    
    a_prop = a;
    t0_prop = t0;
    b = B(pixel);
    g = G(pixel);
    

    a_new = log(exp(a(index))+exp(a(index_2)));
    t0_new = round((t0(index)*exp(a(index))+t0(index_2)*exp(a(index_2)))/exp(a_new));
    strauss=0;
    
    t0_prop(index_2)=[];

    t0_prop(index)=[];

   for j=1:length(t0_prop)
        if abs(t0_new-t0_prop(j))<=max_dist 
            strauss=strauss+log(gamma_strauss);
            break %only if hardconstraint
        end
    end

    if ~isinf(strauss)

        a_prop(index_2)=[];
        a_prop(index)=[];
        
        if isempty(t0_prop)
            t0_prop(1,1) = t0_new;
            a_prop(1,1) = a_new;
        else
            t0_prop(end+1,1) = t0_new;
            a_prop(end+1,1) = a_new;
        end
        
        b_new = b;
        
        u = exp(a(index)-a_new);

        %% proposal logprobability
        prop = compute_likelihood(Y,Y_ind,G,pixel,b_new,t0_prop,a_prop,h,attack,decay);

        [prec,mean_a,det_term] = get_GP_par(t0(index),pixel,A,T0,alpha,scale_Z,Nrow,Nbin,false);
        
        prop= prop-1/2*(a_new-mean_a)^2*prec+det_term;

        %% current logprobability
        curr = compute_likelihood(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay);

        [prec1,mean_a1,det_term1] = get_GP_par(t0(index),pixel,A,T0,alpha,scale_Z,Nrow,Nbin,false);
        [prec2,mean_a2,det_term2] = get_GP_par(t0(index_2),pixel,A,T0,alpha,scale_Z,Nrow,Nbin,false);

        curr = curr - 1/2*(a(index)-mean_a1)^2*prec1+det_term1-1/2*(a(index_2)-mean_a2)^2*prec2+det_term2;


        %% poisson ref meas
        ref_meas = -log(lambda_S)+log(prior_length);

        %% non symmetrical proposal term
        % jacobian u(1-u) (INSIDE BETA PDF FOR SIMPLICITY)
        sym = -log(merge_sum)+log(total_points)+(beta_ms-1+1)*(log(u)+log(1-u))-2*gammaln(beta_ms)+gammaln(2*beta_ms)+log(max_dist+attack+decay);

        %% area interaction
        log_penalty = area_interaction(4,occupied_volume,pixel,t0(index),t0(index_2),t0_new,Npix,Nbin,log_gamma_area_int,lambda_area_int);

        %% accept reject
        if rand<exp(prop-curr+ref_meas+log_penalty+sym)
            A{pixel}=a_prop(:);
            T0{pixel}=t0_prop(:);

            Mergeable_pixels = update_mergeable_list(t0_prop,t0,attack+decay,pixel,Mergeable_pixels);

            total_points=total_points-1;
           %remove pixel from list of length(a) points
           p=PPP{length(a)};
           p(p==pixel)=[];
           PPP{length(a)}=p;
           sum_PPP(length(a))=sum_PPP(length(a))-1;

            if ~isempty(a_prop) %if still has a point increment list
               PPP{length(a_prop)}=[PPP{length(a_prop)};pixel];
               sum_PPP(length(a_prop))=sum_PPP(length(a_prop))+1;
            end

            %% map delta
            map_delta=prop-curr+log_penalty+ref_meas;
            
            %% modify volume
            [occupied_volume,NEIGH,points_with_neigh,neigh_sum] = modify_volume(1,occupied_volume,pixel,[],t0(index_2),Npix,Nbin,NEIGH,T0,index_2,points_with_neigh,neigh_sum);
            [occupied_volume,NEIGH,points_with_neigh,neigh_sum] = modify_volume(1,occupied_volume,pixel,[],t0(index),Npix,Nbin,NEIGH,T0,index,points_with_neigh,neigh_sum);
            [occupied_volume,NEIGH,points_with_neigh,neigh_sum] = modify_volume(0,occupied_volume,pixel,t0_new,[],Npix,Nbin,NEIGH,T0,[],points_with_neigh,neigh_sum);

            eff_prior_length = modify_prior(1,prior_pixels,pixel,t0(index),max_dist,eff_prior_length);
            eff_prior_length = modify_prior(1,prior_pixels,pixel,t0(index_2),max_dist,eff_prior_length);
            eff_prior_length = modify_prior(0,prior_pixels,pixel,find(t0_prop==t0_new),max_dist,eff_prior_length);

            K(pixel,length(a_prop)+1)=K(pixel,length(a_prop)+1)+1;
        else
            K(pixel,length(a)+1)=K(pixel,length(a)+1)+1;
        end
    end
    if isinf(map_delta)
        keyboard
    end
    
end