function run_dataset(filename)

    %% some algorithm parameters
    lowpass_size = 1;
    save_histograms.Nstart = false;
    save_histograms.flag = false;
    init = cputime;
    fig = 20;
    plot_debug = false;
    SBR = 1;
    %% coarse scale     
    load(['data\' filename]);
    Nrow = size(Y,1);
    Ncol = size(Y,2);
    [Y,G_s,Nrow_s,scale_ratio] = downsample_poisson(Y,G,3,Nrow,scale_ratio);
    [Nmc,lambda_area_int,log_gamma_area_int,alpha,alpha_GMRF,Nbin] = get_hyperparam(size(Y,1)*size(Y,2),0,scale_ratio);
    T0_prior = preprocess3D(Y,h,lowpass_size,Nbin);
    output = PointProcess_RJMCMC(Y,h,Nmc,T0_prior,[],[],[],SBR,log_gamma_area_int,lambda_area_int,G_s,alpha,alpha_GMRF,Nrow_s,scale_ratio,Nbin,plot_debug,save_histograms);
    SBR = output{4};
    %% upsample coarse result
    [T0,A,B,T0_prior] = upsample_pointcloud(output{2},output{1},output{3},T0_prior,3,Nrow_s,Nrow,Ncol,scale_ratio,Nbin);
    %% finer scale
    load(['data\' filename]);
    [Nmc,lambda_area_int,log_gamma_area_int,alpha,alpha_GMRF,Nbin] = get_hyperparam(size(Y,1)*size(Y,2),1,scale_ratio);
    output = PointProcess_RJMCMC(Y,h,Nmc,T0_prior,T0,A,B,SBR,log_gamma_area_int,lambda_area_int,G,alpha,alpha_GMRF,Nrow,scale_ratio,Nbin,plot_debug,save_histograms);
    elapsed_time = cputime-init;
        
    %% save results in point cloud format
    [p,intensity_im,depth_im] = results2pcloud(output{2},output{1},scale_ratio,bin_width,Nrow);
    pcwrite(p, ['results\out_' filename '.ply']);
    save(['results\out_' filename],'p','intensity_im','depth_im','elapsed_time')
    
    %% plot 3D reconstruction
    figure(fig)
    pcshow(p);
    
    %% plot error if ground_truth is available
    gth_filename = ['data\ground_truth_' filename];
    if isfile(gth_filename)
       compute_error(gth_filename,output,bin_width,fig+1);
    end
    

end