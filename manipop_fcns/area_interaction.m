function log_prize=area_interaction(type,occupied_volume,n,t0_new,t0_new_2,t0_death,Npix2,Nbin,log_gamma_area_int,lambda_area_int)

% type: indicator of RJ move
% 0 -> birth
% 1 -> death
% 2 -> shift

[Nrow,Ncol,T]=size(occupied_volume);

n_col = ceil(n/Nrow);
n_row = n-(n_col-1)*Nrow;


if type==0 % birth

    b=max([n_col-Npix2,1]):min([n_col+Npix2,Ncol]);
    a=max([n_row-Npix2,1]):min([n_row+Npix2,Nrow]);
    c=max([1,t0_new-Nbin]):min([T,t0_new+Nbin]) ;
    prize=-sum(sum(sum(occupied_volume(a,b,c)==0)));
    
elseif type==1 % death
    
    b=max([n_col-Npix2,1]):min([n_col+Npix2,Ncol]);
    a=max([n_row-Npix2,1]):min([n_row+Npix2,Nrow]);
    c=max([t0_death-Nbin,1]):min([t0_death+Nbin,T]) ;
    prize=sum(sum(sum(occupied_volume(a,b,c)==1)));
    lambda_area_int=-lambda_area_int;
    
elseif type==2 % shift

    b=max([n_col-Npix2,1]):min([n_col+Npix2,Ncol]);
    a=max([n_row-Npix2,1]):min([n_row+Npix2,Nrow]);
    c=max([1,t0_death-Nbin]):min([T,t0_death+Nbin]) ;
    
    prize1=sum(sum(sum(occupied_volume(a,b,c)==1)));
    
    c=max([1,t0_new-Nbin]):min([T,t0_new+Nbin]) ;
    prize2=-sum(sum(sum(occupied_volume(a,b,c)==0)));
    
    if (t0_new-t0_death)<= Nbin*2 && (t0_new-t0_death)>= 0
        c=t0_new-Nbin:t0_death+Nbin;
        prize2=prize2-sum(sum(sum(occupied_volume(a,b,c)==1)));
    elseif (t0_death-t0_new)<= Nbin*2 && (t0_death-t0_new)>= 0   
        c=t0_death-Nbin:t0_new+Nbin;
        prize2=prize2-sum(sum(sum(occupied_volume(a,b,c)==1)));
    end

    prize=prize1+prize2;
    lambda_area_int=0;
    
elseif type==3 %split

    if t0_new>t0_new_2
        aux = t0_new;
        t0_new = t0_new_2;
        t0_new_2 = aux;
    end
    
    b=max([n_col-Npix2,1]):min([n_col+Npix2,Ncol]);
    a=max([n_row-Npix2,1]):min([n_row+Npix2,Nrow]);
    c=max([t0_death-Nbin,t0_new+Nbin+1,1]):min([t0_death+Nbin,t0_new_2-Nbin-1,T]) ;
    prize1=sum(sum(sum(occupied_volume(a,b,c)==1)));
    
    c=max([1,t0_new-Nbin]):max([t0_death-Nbin,t0_new+Nbin,1]);
    prize2=-sum(sum(sum(occupied_volume(a,b,c)==0)));
    
    c=min([t0_death+Nbin,t0_new_2-Nbin,T]):t0_new_2+Nbin;
    prize3=-sum(sum(sum(occupied_volume(a,b,c)==0)));
    
    prize=prize1+prize2+prize3;
    
else %merge 

    if t0_new>t0_new_2
        aux = t0_new;
        t0_new = t0_new_2;
        t0_new_2 = aux;
    end
    
    b=max([n_col-Npix2,1]):min([n_col+Npix2,Ncol]);
    a=max([n_row-Npix2,1]):min([n_row+Npix2,Nrow]);
    c=max([t0_death-Nbin,t0_new+Nbin+1,1]):min([t0_death+Nbin,t0_new_2-Nbin-1,T]) ;
    prize1=-sum(sum(sum(occupied_volume(a,b,c)==0)));
    
    c=t0_new-Nbin:max([t0_death-Nbin,t0_new+Nbin,1]);
    prize2=sum(sum(sum(occupied_volume(a,b,c)==1)));
    
    c=min([t0_death+Nbin,t0_new_2-Nbin,T]):t0_new_2+Nbin;
    prize3=sum(sum(sum(occupied_volume(a,b,c)==1)));
    
    prize=prize1+prize2+prize3;
    lambda_area_int=-lambda_area_int;
    
end
%prize=prize/(Npix2*2+1)^2/(Nbin2*2+1);
prize=prize/(Nbin*2+1);
%prize=2*sigmf(prize,[8e-1 0])-1;

log_prize=prize*log_gamma_area_int+lambda_area_int;