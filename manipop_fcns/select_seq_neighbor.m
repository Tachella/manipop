function  pixel=select_seq_neighbor(Nrow,Ncol,n_row,n_col,iter)

    c = ceil(iter/3);
    r = iter - (c-1)*3;
    r = r - 2;
    c = c - 2;
    if r==0 && c==0
        r=1;
        c=1;
    end
        
    
    n_col=n_col+c;
    n_row=n_row+r;
    
    if n_col>Ncol || n_row>Nrow || n_col<1 || n_row<1
        pixel = 0;
    else
       pixel = n_row + (n_col-1)*Nrow;
    end
end