 function big_prior = extend_prior_PPP(prior_PPP)

big_prior = zeros(sum(prior_PPP),1);

k=1;
for i=1:length(prior_PPP)
    big_prior(k:k+prior_PPP(i)-1)=i;
    k=k+prior_PPP(i);
end   


end