function u_bar=sample_U(b_bar,alpha_GMRF,Nrow)

%map_delta=0;
% 
Ncol=length(b_bar)/Nrow;
% b_bar=reshape(B,Nrow,Nrow);
% b_bar=(circshift(b_bar,[0,1])+circshift(b_bar,[1,0])+circshift(b_bar,[0,-1])+circshift(b_bar,[-1,0]))/4;
% h_filt=ones(3);
% h_filt(2,2)=0;
% h_filt=h_filt/sum(h_filt(:));
% % b_bar=imfilter(b_bar,h_filt,'replicate');
% b_bar=b_bar(:);

U=1./gamrnd(alpha_GMRF,1./(alpha_GMRF*b_bar));

u_bar=reshape(U.^-1,Nrow,Ncol);
u_bar=(u_bar+circshift(u_bar,[0,1])+circshift(u_bar,[1,0])+circshift(u_bar,[0,-1])+circshift(u_bar,[-1,0]))/5;
% h_filt=ones(21);
% h_filt(11,11)=0;
% h_filt=h_filt/sum(h_filt(:));
% u_bar=imfilter(u_bar,h_filt,'replicate');
u_bar=u_bar(:);
%map_delta=sum((-alpha_GMRF-1)*(log(U)-log(last_U))-(b_bar*alpha_GMRF)./(U-last_U));

end