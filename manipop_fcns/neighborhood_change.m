function [neigh_sum,neigh_center]=neighborhood_change(occupied_volume,n,t0_new,Nbin2,NEIGH,T0,neigh_sum)


[Nrow,Ncol,~]=size(occupied_volume);

n_col = ceil(n/Nrow);
n_row = n-(n_col-1)*Nrow;

s=occupied_volume(n_row,n_col,t0_new);
neigh_center = s;
neigh_sum(s) = neigh_sum(s) + 1;
%update neighbors
for iter=1:8
   n_prime=select_seq_neighbor(Nrow,Ncol,n_row,n_col,iter);
   if n_prime
       t0=T0{n_prime};
       for bin=1:length(t0)
           if abs(t0(bin)-t0_new)<=Nbin2
               neigh=NEIGH{n_prime};
               if neigh(bin)
                    neigh_sum(neigh(bin)) = neigh_sum(neigh(bin))-1;
               end
               neigh(bin) = neigh(bin)+1;
               neigh_sum(neigh(bin)) = neigh_sum(neigh(bin))+1;
               break;
           end
       end
   end
end
    
end