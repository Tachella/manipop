function fig = compute_error(filename,output,bin_width,fig)


A = output{1};
T0 = output{2};

load(filename);
A_true = output{1};
T0_true = output{2};

N = length(A);


%% False&True detections
distances = linspace(0,100,10);

T0_false = zeros(length(distances),1);
T0_correct = zeros(length(distances),1);

T0_error = cell(N,1);
A_error = cell(N,1);

for n=1:N
    [T0_error{n},A_error{n}] = find_3D_error(T0_true{n},A_true{n},T0{n},A{n},bin_width);
    
    for d=1:length(distances)
        T0_correct(d) = T0_correct(d) + sum(T0_error{n}<distances(d));
    end
end

for d=1:length(distances)
    T0_false(d) = max([0,sum(cellfun(@length,T0))-T0_correct(d)]);
end


figure(fig)
subplot(121)
plot_3D(T0_true,T0_error,scale_ratio,scale_ratio*5,bin_width,Nrow);
set(gca,'colorscale','log')
title('Depth error [mm]')
subplot(122)
plot_3D(T0_true,A_error,scale_ratio,scale_ratio*5,bin_width,Nrow);
set(gca,'colorscale','log')
title('Intensity error [signal-photons]')
    
fig = fig+1;

total_true = sum(cellfun(@length,T0_true));
figure(fig);
subplot(121)
plot(distances*bin_width,T0_correct/total_true*100);
xlabel('distance [mm]')
ylabel('True points found [%]')
subplot(122)
plot(distances*bin_width,T0_false);
xlabel('distance [mm]')
ylabel('False points')

fig = fig+1;

end