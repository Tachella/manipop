function fig=plot_triang(T0,A,scale_ratio,dot_size,fig,Nrow,bin_width)

%% 3D reconstruction
    figure(fig)
   [intensity,depth] = plot_3D(T0,A,scale_ratio,dot_size,bin_width,Nrow);
   title('3D reconstruction')

    cmap = jet(100);
%% Intensity
    fig=fig+1;
    figure(fig);
    subplot(121)
    imAlpha=ones(size(intensity));
    imAlpha(intensity==0)=0;
    %intensity(intensity==0)=min(intensity(intensity>0));
    imagesc(intensity,'AlphaData',imAlpha);
    set(gca,'color',[1 1 1]); 
    title('intensity')
    axis image
    colormap(cmap)
    colorbar
    axis off
    
%% Depth
    subplot(122)
    imAlpha=ones(size(depth));
    imAlpha(depth==0)=0;
    %depth(depth==0)=min(depth(depth>0));
    imagesc(depth,'AlphaData',imAlpha);
    set(gca,'color',[1 1 1]); 
    colormap('jet')
    title('depth')
    axis image
    axis off
    colorbar
    
    fig = fig+1;
end