function [t0_error,a_error] = find_3D_error(t0_true,a_true,t0,a,bin_width)

t0_error = zeros(size(t0_true));
a_error = t0_error;
for i=1:length(t0_error)
    if isempty(t0)
        t0 = 0;
        a = -Inf;
    end
   [t0_error(i),ind] = min(abs(t0_true(i)-t0));
   a_error = abs(a(ind)-a_true(i));
end
t0_error = t0_error*bin_width;

