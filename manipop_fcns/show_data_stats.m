function [h,attack,decay]=show_data_stats(Y,h,SBR)


    h = h(h>max(h)*0.01);
    [~,attack] = max(h);
    decay = length(h)-attack;
    
    if length(size(Y))==3
       Y = reshape(Y,size(Y,1)*size(Y,2),size(Y,3)); 
    end

    mean_per_pixel = mean(sum(Y,2));
    disp('------------NEW SCALE-------------')
    disp(['mean photon count per pixel ' num2str(mean_per_pixel)])
    disp(['sparsity ' num2str(sum(Y(:)==0)/length(Y(:))*100) '%'])
    disp(['clear pixels ' num2str(sum(sum(Y,2)==0)/size(Y,1)*100) '%'])
    
    h = h(:);
    h = h/max(h);
    
    %% mean value
    mean_value = 20;
    h = h * (mean_per_pixel/(1+1/SBR)/sum(h)/mean_value);
    
end