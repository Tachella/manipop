function [p,intensity_im,depth_im] = results2pcloud(T0,A,scaleZ,bin_width,Nrow)

    tot_points = sum(cellfun(@length,T0));
    T0_plot = zeros(tot_points,2);
    A_plot = zeros(tot_points,1);
    Ncol=length(A)/Nrow;
    intensity_im=zeros(Nrow,Ncol);
    depth_im=zeros(Nrow,Ncol);
    
    k=1;
    min_t0 = Inf;
    for i=1:length(A)
        l=length(T0{i}); T0_plot(k:k+l-1,1) = i;
        T0_plot(k:k+l-1,2) = T0{i};
        if min(T0{i})<min_t0
            min_t0 = min(T0{i});
        end
        if ~isempty(A{i})
            A_plot(k:k+l-1) = A{i};   k=k+l;
            n_col=ceil(i/Nrow);
            n_row=i-Nrow*(n_col-1);
            intensity_im(n_row,n_col) = max(A{i});
            depth_im(n_row,n_col) = min(T0{i});
        end
    end
    T0_plot(:,2) = T0_plot(:,2)-min_t0;
    
    
    x = (T0_plot(:,2))*bin_width;
    y = (Nrow-ceil(T0_plot(:,1)/Nrow))*scaleZ*bin_width;
    z = (Nrow - (T0_plot(:,1) - Nrow*(ceil(T0_plot(:,1)/Nrow)-1)))*scaleZ*bin_width;

    
    p = pointCloud([x(:),y(:),z(:)],'Intensity',A_plot);
        
end