function [A,T0,B,b_bar,map_delta,mdB,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points]=death_move(Y,Y_ind,T,h,attack,decay,A,T0,B,b_bar,alpha_GMRF,lambda_S,gamma_strauss,max_dist,occupied_volume,prior_length,PPP,integrated_h,Npix,Nbin,beta_par,NEIGH,points_with_neigh,neigh_sum,prior_pixels,eff_prior_length,sum_PPP,log_gamma_area_int,lambda_area_int,G,total_points,alpha,scale_Z,K,Nrow,Mergeable_pixels)
    
%%
    mdB=0;
    map_delta = 0;
    
    s_PPP=sum(sum_PPP.*(1:length(sum_PPP))');
    u=rand*s_PPP;
    list=0; csum=0;
    while(u>csum)
        list=list+1;
        csum=csum+sum_PPP(list)*list;
    end
    
    list_pixel=randi(sum_PPP(list));
    p=PPP{list};
    pixel=p(list_pixel);
    
    t0=T0{pixel};  a=A{pixel};
    b=B(pixel);  

    index=randi(list);

    a_prop=a;
    t0_prop=t0;

    a_prop(index)=[];
    t0_prop(index)=[];

    b_new=exp(a(index))*integrated_h/T+b;
    u=b/b_new;
    
    %% proposal logprobability
    prop = compute_likelihood(Y,Y_ind,G,pixel,b_new,t0_prop,a_prop,h,attack,decay);
    
    prop_prior = (alpha_GMRF-1)*log(b_new);
    
    %% current logprobability
    curr = compute_likelihood(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay);
    
    [prec, mean_a, det_term] = get_GP_par(t0(index),pixel,A,T0,alpha,scale_Z,Nrow,Nbin,false);
    curr = curr-(a(index)-mean_a)^2*prec/2+det_term;
    curr_prior = (alpha_GMRF-1)*log(b);
    
    %% other GMRF term
    [b_bar_new,map_GMRF] =  update_b_bar(Nrow,pixel,b_bar,b_new,b,alpha_GMRF);
    
    %% non symmetrical proposal term
    % jacobian (1-u)
    sym=-log(lambda_S)+log(s_PPP)+log(1-u)+log(eff_prior_length-(2*max_dist+1))-log(prior_length); %+(beta_par-1)*log(1-u)+log(beta_par)

    
    %% area interaction
    log_penalty = area_interaction(1,occupied_volume,pixel,[],[],t0(index),Npix,Nbin,log_gamma_area_int,lambda_area_int);

    %% accept reject
    if rand<exp((prop-curr+prop_prior-curr_prior+log_penalty+map_GMRF)+sym)
        A{pixel}=a_prop(:);
        T0{pixel}=t0_prop(:);
        B(pixel)=b_new;
        b_bar=b_bar_new;
        
       Mergeable_pixels = update_mergeable_list(t0_prop,t0,attack+decay,pixel,Mergeable_pixels);
        
       %remove pixel from list of length(a) points
       p=PPP{length(a)};
       p(p==pixel)=[];
       PPP{length(a)}=p;
       sum_PPP(length(a))=sum_PPP(length(a))-1;
        
        if ~isempty(a_prop) %if still has a point increment list
           PPP{length(a_prop)}=[PPP{length(a_prop)};pixel];
           sum_PPP(length(a_prop))=sum_PPP(length(a_prop))+1;
        end

        %% map delta
        map_delta=prop-curr+log_penalty-log(lambda_S/prior_length)+prop_prior-curr_prior+map_GMRF;

        mdB=prop_prior-curr_prior+map_GMRF;
        
        total_points = total_points-1;
        %% modify volume
        [occupied_volume,NEIGH,points_with_neigh,neigh_sum]=modify_volume(1,occupied_volume,pixel,[],t0(index),Npix,Nbin,NEIGH,T0,index,points_with_neigh,neigh_sum);
        eff_prior_length=modify_prior(1,prior_pixels,pixel,t0(index),max_dist,eff_prior_length);
        
        K(pixel,length(a_prop)+1)=K(pixel,length(a_prop)+1)+1;
    else
        K(pixel,length(a)+1)=K(pixel,length(a)+1)+1;
    end
    
%     if isreal(map_delta)==0 || isinf(map_delta)
%         keyboard
%     end
    
end