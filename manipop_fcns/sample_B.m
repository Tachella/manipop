function [B_new,new_b_bar,map_delta,mdB] = sample_B(Y,Y_ind,T,h,attack,decay,A,T0,B,b_bar,alpha_GMRF,u_bar,G,Nrow)

N = length(Y);
B_new = zeros(N,1);
Ncol = N/Nrow;
map_delta = 0;

%% sample pixels
for n=1:N 
    if G(n)>0
        ind=Y_ind{n};
        a=A{n}; t0=T0{n};
        x_curr=zeros(length(ind),1);
        for j=1:length(t0)
            i1 = find(ind>t0(j)-attack & ind<t0(j)+decay);
            if ~isempty(i1)
                x_curr(i1)=x_curr(i1)+G(n)*exp(a(j))*h(ind(i1)-t0(j)+attack);
            end
        end
        y=Y{n};
        ybn = sum(y(x_curr==0));
        if ~(length(ind)==1 && sum(x_curr>0)>0)
            ybn = ybn + sample_photon_sum(y(x_curr>0)',x_curr(x_curr>0),G(n)*B(n));
        end
        B_new(n) = gamrnd((alpha_GMRF+ybn),1./(G(n)*T+alpha_GMRF*u_bar(n)));
        map_delta = map_delta +  y*(log(B_new(n)+x_curr)-G(n)*log(B(n)+x_curr));
    else
        B_new(n) = gamrnd((alpha_GMRF),1./(alpha_GMRF*u_bar(n)));
    end
end

%% new b_bar
x = reshape(B_new,Nrow,Ncol);
new_b_bar = (x + circshift(x,[1,0])+circshift(x,[-1,0])+circshift(x,[0,1])+circshift(x,[0,-1]))/5;
new_b_bar = new_b_bar(:);

%% MAP delta computation 
map_delta = map_delta - T*sum(B_new-B);
mdB = sum((alpha_GMRF-1)*(log(B_new)-log(B))-alpha_GMRF*(log(new_b_bar)-log(b_bar)));
map_delta= map_delta +mdB;

end