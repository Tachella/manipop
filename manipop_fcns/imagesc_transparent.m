function imagesc_transparent(intensity,alims)

    imAlpha=ones(size(intensity));
    imAlpha(intensity==0)=0;
    intensity(intensity==0)=min(intensity(intensity>0));
    imagesc(intensity,'AlphaData',imAlpha,alims);
    set(gca,'color',[1 1 1]); 
    title('intensity')
    axis image
    colormap(jet(100))
%     colorbar
    axis off

end