function  [n_col,n_row] = select_seq_neighbor_3(Nrow,Ncol,n_row,n_col,iter)

    pos = [1,1;1,0;1,-1;0,-1;-1,-1;-1,0;-1,1;0,1];
    
    n_col=n_col+pos(iter,1);
    n_row=n_row+pos(iter,2);
    if n_col>Ncol || n_row>Nrow || n_col<1 || n_row<1
        
        n_col=0; 
        
    end
end