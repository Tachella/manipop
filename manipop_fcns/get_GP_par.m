function  [prec,mean_a,det_term]=get_GP_par(t0_new,pixel,A,T0,alpha,scale_Z,Nrow,Nbin,mark_move)

    det_term = 0;
    N=length(A);
    neighs = 8;
    Ncol=N/Nrow;
    
    n_col = ceil(pixel/Nrow);
    n_row = pixel - (n_col-1)*Nrow;
    
    X_ref = zeros(3,1);
    X_ref(1)= t0_new/scale_Z;
    X_ref(2)= n_row;
    X_ref(3)= n_col;
%   constant_distance = 1;
    beta = alpha/100;
    
    a_neigh = zeros(neighs,1);
    dist_neigh = zeros(neighs,1);
    Z = zeros(neighs,1);
    X = zeros(3,1);
    
    neigh_flag = false;
    %% get neighbors
    for iter=1:neighs           
       [neigh_col, neigh_row] = select_seq_neighbor_3(Nrow, Ncol, n_row, n_col, iter);
       Z(iter) = 1e10;
       dist_neigh(iter) = 1e15;
       if neigh_col
          pix = neigh_row + (neigh_col-1)* Nrow;
          t0 = T0{pix}; a = A{pix};
          for j=1:length(t0)
              if abs(t0(j)-t0_new) <= 2*Nbin
                  X(1) = t0(j)/scale_Z;
                  X(2) = neigh_row;
                  X(3) = neigh_col;
                  Z(iter) = X(1);
                  a_neigh(iter) = a(j);
                  dist_neigh(iter) = sqrt(sum((X_ref-X).^2));
%                 dist_neigh(iter) = constant_distance;
                 neigh_flag = true;
              end
          end
       end
    end

    %% precision matrix
    if ~neigh_flag
        prec = beta/alpha;
        mean_a = 0;  
        det_term = 1/2*(log(prec)-log(2*pi));
    else
        
        p = -1./dist_neigh;
        pp = beta - sum(p);

        prec = pp/alpha;
        mean_a = (-sum(a_neigh.*p))/pp;
        
        if mark_move == false
             DIST = ([Z-circshift(Z,1),Z-circshift(Z,-1),Z-circshift(Z,2),Z-circshift(Z,-2)]).^2;
             DIST(:,1:2) = sqrt((DIST(:,1:2)+1));
             DIST(:,3:4) = sqrt((DIST(:,3:4)+2));

            P_approx = zeros(8);
            for i=1:8
                P_approx(mod(i-2,8)+1,i) = -1/DIST(i,1);
                P_approx(mod(i,8)+1,i) = -1/DIST(i,2);
%                 P_approx(mod(i-2,8)+1,i) = -1/constant_distance;
%                 P_approx(mod(i,8)+1,i) = -1/constant_distance;


                if rem(i,2)==0
                     P_approx(mod(i-3,8)+1,i) = -1/DIST(i,3);
                     P_approx(mod(i+1,8)+1,i) = -1/DIST(i,4);
%                      P_approx(mod(i-3,8)+1,i) = -1/constant_distance;
%                      P_approx(mod(i+1,8)+1,i) = -1/constant_distance;
                end
                P_approx(i,i) = beta - sum(P_approx(:,i));
            end

            
            old_det = log(det(P_approx));

            P_approx = P_approx - diag(p);
            P_approx = [P_approx,p;p',pp];
            
            det_term = 1/2*(log(det(P_approx)) - log(alpha) - log(2*pi) - old_det - ( sum(a_neigh.^2.*(-p)) - pp*mean_a^2)/alpha);
            
            %disp(1/sqrt(prec)/abs(mean_a))
        end
    
    end
end