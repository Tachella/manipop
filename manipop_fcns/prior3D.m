function prior=prior3D(y,h,attack,decay,lowpass_size,Nrow)

[N,T] = size(y);

Ncol = N/Nrow;

prior = log_matched_filter(h,attack,decay,y);


if lowpass_size>=1
    prior=reshape(prior,Nrow,Ncol,T);

%h=ones(lowpass_size,lowpass_size,lowpass_size*(attack+decay)/2);
%h=h/sum(h(:));

    sigma=2;
    prior=imgaussfilt3(prior,sigma);
    prior=reshape(prior,N,T);

end

end
