function x=log_matched_filter(h,attack,decay,y)

   % t=-attack:decay;
    
   % imp=h(t);
    
   %log matched filtering
   f=ones(1,length(h));
   f(1,:)=(log(h)-min(log(h)))/sum(log(h)-min(log(h)));
   T=size(y,2);
%  x=imfilter(y,fliplr(f),'symmetric','full');
   x = conv2(fliplr(f),y);
   x = x(:,decay:T+decay-1);
   
   
    %remove group delay
    %x=circshift(x,[0,-1.5*attack]);
    
  end