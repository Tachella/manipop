function [MSE_error,delta_points]=reconstruction_error(T0_true,A_true,B_true,T0,A,B,h,attack,decay,T,Nrow,G)

Ncol=length(T0)/Nrow;

MSE_error = 0;
for n=1:Nrow*Ncol
    
    %% true signal
    t0 = T0_true{n}; a = A_true{n};
    x_true = zeros(T,1);
    for j=1:length(t0)
        ind=t0(j)-attack:t0(j)+decay-1;
        x_true(ind)=x_true(ind)+G(n)*a(j)*h;
    end
    x_true = x_true+ B_true(n);
    
    %% estimated signal
    t0 = T0{n}; a = A{n};
    x_est=zeros(T,1);
    for j=1:length(t0)
        ind=t0(j)-attack:t0(j)+decay-1;
        x_est(ind)=x_est(ind)+G(n)*a(j)*h;
    end
    x_est = x_est + B(n);
    
    %% MSE
    MSE_error = MSE_error + (x_est - x_true).^2;
    
end

MSE_error = sqrt(MSE_error/Nrow/Ncol);
delta_points = sum(cellfun(@length,T0))-sum(cellfun(@length,T0_true));

%pcshowpair(ptCloudA,ptCloudB)

end