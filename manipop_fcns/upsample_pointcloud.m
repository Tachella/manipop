function [T0_new,A_new,B_new,T0_prior_new,scale_ratio]=upsample_pointcloud(T0,A,B,T0_prior,factor,Nrow,Nrow_prime,Ncol_prime,scale_ratio,Nbin)


if factor>1
    strauss = 2*Nbin+3;
    N=length(T0);
    Ncol=N/Nrow;
    N_prime=Nrow_prime*Ncol_prime;
    A_new = cell(N_prime,1);
    T0_new = cell(N_prime,1);
    B_new = zeros(N_prime,1);
    T0_prior_new = cell(N_prime,1);
    
    for n=1:N
        n_col = ceil(n/Nrow);
        n_row = n-(n_col-1)*Nrow;
        n_col_prime = n_col*factor-1;
        n_row_prime = n_row*factor-1;
        
        
        t0=T0{n};
        a=A{n}/factor^2;
        prior = T0_prior{n};
        for i=-floor(factor/2):floor(factor/2)
            for j=-floor(factor/2):floor(factor/2)
                if (n_col_prime+i)<=Ncol_prime && (n_row_prime+j<Nrow_prime)
                    
                    n_prime = (n_col_prime+i-1)*Nrow_prime+n_row_prime+j;
                    
                    %% upsample prior
                    T0_prior_new{n_prime}=prior;
                    
                    %% interpolate
                    if (i==0 && j==0) || n_col==Ncol || n_row==Nrow || n_col==1 || n_row==1
                        A_new{n_prime} = a;
                        T0_new{n_prime} = t0;
                    else
                        n_neighbor = (n_col+i-1)*Nrow+n_row+j; % corregir i y j (si el factor no es 3)
                        
                        t0_neigh = T0{n_neighbor};
                        t0_new = t0; a_new = a;
                        for r=1:length(t0)
                            for p=1:length(t0_neigh)
                                if abs(t0_neigh(p)-t0(r))<=Nbin
                                    a_neigh = A{n_neighbor}/factor^2;
                                    
                                    a_new(r) = (a(r)*2/factor+a_neigh(p)/factor); % corregir i y j (si el factor no es 3)
                                    t0_new(r) = round(t0(r)*2/factor+t0_neigh(p)/factor);
                                end
                            end
                        end
                        A_new{n_prime}=a_new(:);
                        T0_new{n_prime}=t0_new(:);
                        
                    end
                    B_new(n_prime)=B(n)/factor^2;
                end
            end
        end
    end
    
    for n_prime=1:N_prime
        
        a_new=A_new{n_prime};
        t0_new=T0_new{n_prime};
        
        if length(t0_new)>1
            g=1;
            while g<length(t0_new)
                if abs(t0_new(g+1)-t0_new(g))<=strauss
                    t0_new(g)=[];
                    a_new(g)=[];
                end
                g=g+1;
            end
        end
        A_new{n_prime}=a_new(:);
        T0_new{n_prime}=t0_new(:);
    end
    
    scale_ratio=scale_ratio/factor;
    
else
    T0_new=T0;
    A_new=A;
    B_new=B;
    
end

end