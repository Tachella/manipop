function [X,G_new,Nrow,scale_ratio]=downsample_poisson(Y,G,factor,Nrow,scale_ratio)

if length(size(Y))==2
    Ncol = size(Y,1)/Nrow;
    Y = reshape(Y,Nrow,Ncol,T);
else
    Ncol = size(Y,2);
end
    
if isempty(G)
   G=ones(Nrow,Ncol); 
elseif size(G,2)==1
   G = reshape(G,Nrow,Ncol);
end

if factor>1
   
    Nrow_old = Nrow;
    Ncol_old = Ncol;
    Nrow=ceil(Nrow/factor);
    Ncol=ceil(Ncol/factor);
    
    X = zeros(Nrow,Ncol,size(Y,3));
    G_new = zeros(Nrow,Ncol);
    
    k=1; p=1;
    for i=1:Nrow
        idx = 1+(i-1)*factor:min([i*factor,Nrow_old]);
        for j=1:Ncol
            idy = 1+(j-1)*factor:min([j*factor,Ncol_old]);
            X(k,p,:) = sum(sum(Y(idx,idy,:),2),1);
            G_new(k,p) = sum(sum(G(idx,idy)))/factor^2;
            p = p+1;
        end
        p=1;
        k=k+1;
    end
    
    scale_ratio=scale_ratio*factor;
    
end

end