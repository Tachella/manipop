function [T0,map_delta,occupied_volume,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,Mergeable_pixels]=shift_move(Y,Y_ind,h,attack,decay,A,T0,B,T0_prior,gamma_strauss,max_dist,occupied_volume,PPP,Npix,Nbin,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,log_gamma_area_int,lambda_area_int,G,ppp,alpha,scale_Z,Nrow,T,Mergeable_pixels)
    
    map_delta=0;
    sigma_RWM = Nbin/2;
%%
  
    u=rand*sum(ppp.*(1:length(ppp))');
    list=0; csum=0;
    while(u>csum)
        list=list+1;
        csum=csum+ppp(list)*list;
    end
    
    list_pixel=randi(ppp(list));
    p=PPP{list};
    pixel=p(list_pixel);

    
    t0=T0{pixel};  a=A{pixel};
    b=B(pixel);   
    
    index=randi(length(a));
    
    a_prop=a;
    t0_prop=t0;
    
    while t0_prop(index)==t0(index)
        t0_prop(index)=round(normrnd(t0(index),sigma_RWM));
    end
    
    if sum((t0_prop(index)-T0_prior{pixel})==0) %if the proposal lies inside the prior
       
        
        %% current logprobability
        curr = compute_likelihood(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay);
        
        [prec_prior,mean_a,det_term1]=get_GP_par(t0(index),pixel,A,T0,alpha,scale_Z,Nrow,Nbin,false);
        
        prior_curr = -1/2*(a(index)-mean_a)^2*prec_prior + det_term1;
        %% proposal logprobability
        prop = compute_likelihood(Y,Y_ind,G,pixel,b,t0_prop,a_prop,h,attack,decay);
        
        %% GP prior        
        [prec,mean_a,det_term2] = get_GP_par(t0_prop(index),pixel,A,T0,alpha,scale_Z,Nrow,Nbin,false);
        
        prior_prop = -1/2*(a(index)-mean_a)^2*prec + det_term2;
        
        %% strauss term
        strauss1=0; 
        strauss2=0;
        for j=1:length(t0)
            if j~=index
                if abs(t0(index)-t0(j))<=max_dist
                    strauss1 = strauss1 + log(gamma_strauss);
                end
                if abs(t0_prop(index)-t0(j))<=max_dist
                    strauss2 = strauss2 + log(gamma_strauss);
                end
            end
        end
        
        %% area interaction
        log_penalty = area_interaction(2,occupied_volume,pixel,t0_prop(index),[],t0(index),Npix,Nbin,log_gamma_area_int,lambda_area_int);

        %% accept reject
        if rand<exp(prop-curr+prior_prop-prior_curr-strauss1+strauss2+log_penalty)
            T0{pixel}=t0_prop(:);
            
            
            Mergeable_pixels = update_mergeable_list(t0_prop,t0,attack+decay,pixel,Mergeable_pixels);
            
            map_delta=prop-curr-strauss1+strauss2+log_penalty+prior_prop-prior_curr;
            [occupied_volume,NEIGH,points_with_neigh,neigh_sum]=modify_volume(2,occupied_volume,pixel,t0_prop(index),t0(index),Npix,Nbin,NEIGH,T0,index,points_with_neigh,neigh_sum);
            
            eff_prior_length = modify_prior(1,T0_prior,pixel,t0(index),max_dist,eff_prior_length);
            eff_prior_length = modify_prior(0,T0_prior,pixel,t0_prop(index),max_dist,eff_prior_length);
        end
    end
    
end