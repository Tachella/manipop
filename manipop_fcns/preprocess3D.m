function [T0_prior,T0,A,B] = preprocess3D(Y,h,lowpass_size,Nbin)


    h = h(h>max(h)*0.01);
    [~,attack] = max(h);
    decay = length(h)-attack;    
    Nrow = size(Y,1);
    Y = reshape(Y,size(Y,1)*size(Y,2),size(Y,3));
    [N,T]=size(Y);
    t_min=max([attack+1,Nbin+1]);
    t_max=min([T-decay-1,T-Nbin-1]);
    
 
    %% match filtering
    t_prior = prior3D(Y,h,attack,decay,lowpass_size,Nrow);
    
    integrated_h = sum(h);
    %% thresholding prior
   % constant = mean(t_prior,2);
    
   % m = sum(log(h)-min(log(h)))/T+constant;
    
   % t_prior(t_prior<m)=0;
    
  %  constant = mean(constant)/2;
    m = 0.008*sum(Y,2)/length(h);
    
    t_prior(t_prior<m)=0;
    t_prior(:,1:t_min)=0;
    t_prior(:,t_max:end)=0;
    
    A = cell(N,1);
    T0 = cell(N,1);
    B = ones(N,1);
    
    T0_prior = cell(N,1);
   % prior_length=0;
    for n=1:N
       T0_prior{n} = find(t_prior(n,:)>0)';
      %T0_prior{n} = (t_min:t_max)';
      %  prior_length = prior_length+length(T0_prior{n});
         [~,t0]=max(t_prior(n,:));
        a=zeros(size(t0));
        b=sum(Y(n,:));
        for i=1:length(t0)
            a(i) = sum(Y(n,max([1,t0(i)-attack]):min([t0(i)+decay-1,size(Y,2)])))/integrated_h;
            b = b - a(i)*integrated_h;
        end
        if a>0
            A{n}=log(a(:));
            T0{n} = t0(:);
        end
        B(n)=b/size(Y,2);
    end
    
    
    %% space to explore
    disp(['space to explore ' num2str(sum(sum(cellfun(@length,T0_prior)))/N/T*100) '%'])

end