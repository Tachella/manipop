function [A,T0,B,b_bar,map_delta,mdB,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points]=erode_move(Y,Y_ind,T,h,attack,decay,A,T0,B,b_bar,alpha_GMRF,NEIGH,lambda_S,gamma_strauss,max_dist,occupied_volume,prior_length,PPP,total_points,integrated_h,Npix,Nbin,points_with_neigh,neigh_sum,prior_pixels,eff_prior_length,sum_PPP,log_gamma_area_int,lambda_area_int,G,alpha,scale_Z,K,Nrow,Mergeable_pixels)
    
    mdB=0;
    map_delta=0;
    N=length(Y);
    
    %% pick a pixel and bin from ones with neighbors
    accepted=false;
    s_PPP=sum(sum_PPP.*(1:length(sum_PPP))');
    while ~accepted
        u=rand*s_PPP;
        list=0; csum=0;
        while(u>csum)
            list=list+1;
            csum=csum+sum_PPP(list)*list;
        end
        list_pixel=randi(sum_PPP(list));
        p=PPP{list};
        pixel=p(list_pixel);
        index=randi(list);
        neigh=NEIGH{pixel};
        if neigh(index)
            accepted=true;
        end
    end
    
        
    %% kill the peak
    t0 = T0{pixel};
    a = A{pixel};
    b = B(pixel);
    
    a_prop=a;
    t0_prop=t0;

    a_prop(index)=[];
    t0_prop(index)=[];

    b_new = exp(a(index))*integrated_h/T+b;
    
    %% proposal logprobability
    prop = compute_likelihood(Y,Y_ind,G,pixel,b_new,t0_prop,a_prop,h,attack,decay);
    prop_prior = (alpha_GMRF-1)*log(b_new);
    
    %% current logprobability
    curr = compute_likelihood(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay);

    [prec, mean_a, det_term] = get_GP_par(t0(index),pixel,A,T0,alpha,scale_Z,Nrow,Nbin,false);
    curr = curr -1/2*prec*(mean_a-a(index))^2 + det_term;
    curr_prior = (alpha_GMRF-1)*log(b);
    
    %% other GMRF term
    [b_bar_new,map_GMRF] =  update_b_bar(Nrow,pixel,b_bar,b_new,b,alpha_GMRF);
    
    %% non sym dilation term
    n_col = ceil(pixel/Nrow);
    n_row = pixel-(n_col-1)*Nrow;
    total_neigh = occupied_volume(n_row,n_col,t0(index));
    term=-log((total_points+1)/(total_neigh-1))-log(2*Nbin+1);
    
    %% Poisson ref measure
    ref_measure = -log(lambda_S) + log(prior_length);
    
    %% non symmetrical proposal term
    sym =  term ...  % dilation proposal term 
     + log(points_with_neigh) ...% erosion proposal term 
     + 1/2*(log(prec)-log(2*pi)-prec*(a(index)-mean_a)^2); 

    %% strauss term
    strauss=0;
    for j=1:length(t0_prop)
        if abs(t0(index)-t0_prop(j))<=max_dist
            strauss=strauss-log(gamma_strauss);
        end
    end
    
    %% area interaction
    log_penalty = area_interaction(1,occupied_volume,pixel,[],[],t0(index),Npix,Nbin,log_gamma_area_int,lambda_area_int);

    %% accept reject
    if rand<exp(ref_measure+prop-curr+prop_prior-curr_prior+strauss+log_penalty+map_GMRF+sym)
        A{pixel}=a_prop(:);
        T0{pixel}=t0_prop(:);
        B(pixel)=b_new;
        b_bar=b_bar_new;
        
        Mergeable_pixels = update_mergeable_list(t0_prop,t0,attack+decay,pixel,Mergeable_pixels);
        
        %remove pixel from list of length(a) points
        p=PPP{length(a)};
        p(p==pixel)=[];
        PPP{length(a)}=p;
        sum_PPP(length(a))=sum_PPP(length(a))-1;

        
        if ~isempty(a_prop) %if still has a point increment list
           PPP{length(a_prop)}=[PPP{length(a_prop)};pixel];
           sum_PPP(length(a_prop))=sum_PPP(length(a_prop))+1;
        end

        %% map delta
        map_delta=prop-curr+strauss+log_penalty+ref_measure + prop_prior-curr_prior+map_GMRF;
        mdB=prop_prior-curr_prior+map_GMRF;
        
        %% modify volume
        [occupied_volume,NEIGH,points_with_neigh,neigh_sum]=modify_volume(1,occupied_volume,pixel,[],t0(index),Npix,Nbin,NEIGH,T0,index,points_with_neigh,neigh_sum);
        eff_prior_length=modify_prior(1,prior_pixels,pixel,t0(index),max_dist,eff_prior_length);
    
        total_points=total_points-1;
        K(pixel,length(a_prop)+1)=K(pixel,length(a_prop)+1)+1;
    else
        K(pixel,length(a)+1)=K(pixel,length(a)+1)+1;
    end
    
end