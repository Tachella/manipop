function [a_samples,t0_samples]=build_histogram(T0,A,B,G,pixel,index,Nmc)
    
%%
    a_samples = zeros(Nmc,1);
    t0_samples = zeros(Nmc,1);
    sigma_RWM = 0.5;
    
    t0=T0{pixel};  a=A{pixel};
    b=B(pixel);
    g=G(pixel);
    
    
    ind=Y_ind{pixel};
    
    logical_ind=ind>t0(index)-attack & ind<t0(index)+decay;
    ind=ind(logical_ind);
    
    x=zeros(length(ind),1);
    
    y = Y{pixel};
    y = y(logical_ind);

    
    
    for i=1:Nmc
        
        [prec, mean_a] = get_GP_par(t0(index), pixel, A, T0, alpha, scale_Z, Nrow, Nbin, true);
    
    %% current log probability
        for j=1:length(t0)
            i1=find(ind>t0(j)-attack & ind<t0(j)+decay);
            x(i1)=x(i1)+g*exp(a(j))*h(ind(i1)-t0(j)+attack);
        end
        curr=g*exp(a(index))*h(ind-t0(index)+attack);
        x_curr=x+b;
        prior_curr=-1/2*(a(index)-mean_a)^2*prec;


        %% proposal log_prob
        a_prop=a;
        a_prop(index) = a(index)+randn*sigma_RWM;

        prop = g*exp(a_prop(index))*h(ind-t0(index)+attack);
        x_prop = x+prop-curr+b;
        prior_prop = -1/2*(a_prop(index)-mean_a)^2*prec;

        %% accept-reject
        likelihood=y*(log(x_prop)-log(x_curr))-(exp(a_prop(index))-exp(a(index)))*g*integrated_h;
        if exp(likelihood + prior_prop - prior_curr)>rand
            a = a_prop(:);
        end
        
        
    a_prop=a;
    t0_prop=t0;
    
    while t0_prop(index)==t0(index)
        t0_prop(index)=round(normrnd(t0(index),sigma_RWM));
    end
    
    if sum((t0_prop(index)-T0_prior{pixel})==0) %if the proposal lies inside the prior
        
        if t0_prop(index) > t0(index)
            t0_min = t0(index);
            t0_max = t0_prop(index);
        else
            t0_min = t0_prop(index);     
            t0_max = t0(index);       
        end
        
        logical_ind = ind>t0_min-attack & ind<t0_max+decay;
        ind = ind(logical_ind);
        y = y(logical_ind);
        %% current logprobability
        x=zeros(length(ind),1);
        for j=1:length(t0)
            i1=find(ind>t0(j)-attack & ind<t0(j)+decay);
            x(i1)=x(i1)+g*exp(a(j))*h(ind(i1)-t0(j)+attack);
        end
        y_curr=x+b;

        curr=y*log(y_curr);
        
        [prec_prior,mean_a,det_term1]=get_GP_par(t0(index),pixel,A,T0,alpha,scale_Z,Nrow,Nbin,false);
        
        prior_curr = -1/2*(a(index)-mean_a)^2*prec_prior + det_term1;
        %% proposal logprobability
        x=zeros(length(ind),1);
        for j=1:length(t0_prop)
            i1=find(ind>t0_prop(j)-attack & ind<t0_prop(j)+decay);
            x(i1)=x(i1)+g*exp(a_prop(j))*h(ind(i1)-t0_prop(j)+attack);
        end
        y_prop=x+b;

        prop=y*log(y_prop);
        
        %% GP prior        
        [prec,mean_a,det_term2] = get_GP_par(t0_prop(index),pixel,A,T0,alpha,scale_Z,Nrow,Nbin,false);
        
        prior_prop = -1/2*(a(index)-mean_a)^2*prec + det_term2;
        
        %% strauss term
        strauss1=0; 
        strauss2=0;
        for j=1:length(t0)
            if j~=index
                if abs(t0(index)-t0(j))<=max_dist
                    strauss1 = strauss1 + log(gamma_strauss);
                end
                if abs(t0_prop(index)-t0(j))<=max_dist
                    strauss2 = strauss2 + log(gamma_strauss);
                end
            end
        end
        
        %% area interaction
        log_penalty = area_interaction(2,occupied_volume,pixel,t0_prop(index),[],t0(index),Npix,Nbin,log_gamma_area_int,lambda_area_int);

        %% accept reject
        if rand<exp(prop-curr+prior_prop-prior_curr-strauss1+strauss2+log_penalty)
            T0{pixel}=t0_prop(:);
            
            
            Mergeable_pixels = update_mergeable_list(t0_prop,t0,attack+decay,pixel,Mergeable_pixels);
            
            map_delta=prop-curr-strauss1+strauss2+log_penalty+prior_prop-prior_curr;
            [occupied_volume,NEIGH,points_with_neigh,neigh_sum]=modify_volume(2,occupied_volume,pixel,t0_prop(index),t0(index),Npix,Nbin,NEIGH,T0,index,points_with_neigh,neigh_sum);
            
            eff_prior_length = modify_prior(1,T0_prior,pixel,t0(index),max_dist,eff_prior_length);
            eff_prior_length = modify_prior(0,T0_prior,pixel,t0_prop(index),max_dist,eff_prior_length);
        end
    end
    
end

end