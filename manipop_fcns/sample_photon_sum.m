function phot = sample_photon_sum(y,x,b)

    p = b./(x+b);
    
    if length(y)<15
        phot = sum(binornd(y,p));
    else
%         phot = yp + sqrt(y'*(p.*(1-p))) *randn;
        phot = poissrnd(y'*p);
    end

end