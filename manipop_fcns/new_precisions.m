function [prec,p,mean_a]=new_precisions(x_new,X,P,A_mat,total_points)

alpha=0.0005;
anisotropy=[1;1;1/20];

if isempty(X)
    prec=1/alpha;
    p=[];
    mean_a=0;
else
    k=alpha*exp(-abs(x_new-X)*anisotropy);
    %k(k<alpha*1e-3)=0;
    k=sparse(k);
    aux=P(1:total_points-1,1:total_points-1)*k;
   
    %aux(aux<1e-5)=0;    
    prec=1/(alpha-k'*aux);
    p=-aux*prec;
    %p(p>-sqrt(prec))=0;
    nnz(p)
    
    mean_a = -p'/prec*A_mat;
    
    
    if prec<0
           keyboard 
    end
end


end