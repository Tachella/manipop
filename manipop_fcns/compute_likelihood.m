function curr = compute_likelihood(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay)

g = G(pixel);
curr = 0;
if g>0
    ind = Y_ind{pixel};
    x = g*b*ones(length(ind),1);
    for j=1:length(t0)
        i1=find(ind>t0(j)-attack & ind<t0(j)+decay);
        x(i1)=x(i1)+g*exp(a(j))*h(ind(i1)-t0(j)+attack);
    end
    curr = curr + Y{pixel}*log(x);
end

end