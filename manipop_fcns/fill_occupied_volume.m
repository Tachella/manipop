function occupied_volume=fill_occupied_volume(T0,T,Npix2,Nbin2,Nrow)

N=length(T0);
Ncol=N/Nrow;

occupied_volume=zeros(Nrow,Ncol,T);


for n=1:length(T0)
    
    n_col = ceil(n/Nrow);
    n_row = n -(n_col-1)*Nrow;
    b=max([n_col-Npix2,1]):min([n_col+Npix2,Ncol]);
    a=max([n_row-Npix2,1]):min([n_row+Npix2,Nrow]);
    
    t0=T0{n};
    for k=1:length(t0)
        t0_new=t0(k);
        c=t0_new-Nbin2:t0_new+Nbin2 ;
        occupied_volume(a,b,c)=occupied_volume(a,b,c)+1;   
    end
end


end