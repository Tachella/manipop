function b=is_border(n,Nrow,Ncol)

n_col=ceil(n/Nrow);
n_row=n-(n_col-1)*Nrow;

b = (n_col==1) + (n_col==Ncol) + (n_row==1) + (n_row==Nrow);

end