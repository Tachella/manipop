function [A,T0] = remove_lonely_points(A,T0,NEIGH)

N = length(A);

for n=1:N
    neigh = NEIGH{n};
    a = A{n}; j=1;
    points = length(a);
    while(j<=points)
        if neigh(j)==0 || exp(a(j))<0.01
            t0 = T0{n};
            a(j) = []; t0(j) = [];
            neigh(j) = [];
            A{n} = a(:); T0{n} = t0(:);
            NEIGH{n} = neigh(:);
            j = j-1;
            points = points - 1;
        end
        j = j+1;
    end
end

end