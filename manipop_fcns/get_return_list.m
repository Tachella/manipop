function [Y_out,Y_ind,T] = get_return_list(Y)

if length(size(Y))==3
   Y = reshape(Y,size(Y,1)*size(Y,2),size(Y,3)); 
end

[N,T]=size(Y);

Y_out = cell(N,1);
Y_ind = cell(N,1);

for n=1:N
   Y_ind{n} = find(Y(n,:));
   Y_out{n} = Y(n,Y_ind{n});
end


end