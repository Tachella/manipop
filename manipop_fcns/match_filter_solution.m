close all
clear all
clc

%% load stuff
filename = 'image_40m_F100_1ms.mat';
%filename = 'camouflage_3.2ms.mat';
%filename = 'image_325m_F500_R50_2plates_1_0.1ms.mat';
%filename = 'college_long.mat';
%filename = '620nm_2_300x300_0.2ms.mat';
%filename = 'cottage_full.mat';
%filename = 'multihead_30ms_0deg.mat';

load(['data/' filename])
tic
h = show_data_stats(Y,Y_ind,T,h);

Y = get_full_3D_mat(Y,Y_ind,T);

Ncol = size(Y,1)/Nrow;

t_prior=prior3D(Y,h,attack,decay,0,Nrow);

h = h;
integrated_h=sum(h);
A = cell(Nrow*Ncol,1);
T0 = cell(Nrow*Ncol,1);
B =zeros(Nrow*Ncol,1);
%%
% Nbin=50;
% m = 0.05*(log(h)-min(log(h)))'*h;
% t_prior(t_prior<m)=0;
% t_prior(:,1:attack+1)=0;
% t_prior(:,end-decay-1:end)=0;


for n=1:Nrow*Ncol

    %[~,t0]=findpeaks(t_prior(n,1:end),'MinPeakDistance',Nbin*2+5);
    [~,t0]=max(t_prior(n,:));
    a=zeros(size(t0));
    b=sum(Y(n,:));
    for i=1:length(t0)
        a(i) = sum(Y(n,max([1,t0(i)-attack]):min([t0(i)+decay-1,size(Y,2)])))/integrated_h;
        b = b - a(i)*integrated_h;
    end
    if a>0
        A{n}=log(a(:));
        T0{n} = t0(:);
    end
    B(n)=b/size(Y,2);
end
elapsed_time=toc;
figure(1)
imagesc(reshape(B,Nrow,Ncol));
axis image
axis off
colorbar


output{1} = A;
output{2} = T0;
output{3} = B;
output{4} = [];
output{5} = [];
output{6} = [];
output{7} = B;
output{8} = h;

scaleA = sum(h);
bin_width = 1.2;
fig = 10;
fig=plot_triang(output{2},output{1},scale_ratio,scale_ratio*5,fig,Nrow,bin_width,scaleA);

save(['results\output_log_matched_' filename],'output','Nrow','scale_ratio','elapsed_time')
