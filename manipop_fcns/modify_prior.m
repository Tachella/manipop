function eff_prior_length=modify_prior(type,prior_pixels,pixel,t0,max_dist,eff_prior_length)


if type==0 %birth
    
    p=prior_pixels{pixel};
    
    counter=length(p(p>=t0-max_dist & p<=t0+max_dist));
    
    eff_prior_length=eff_prior_length-counter;
    
else % death
    
    p=prior_pixels{pixel};
    
    counter=length(p(p>=t0-max_dist & p<=t0+max_dist));
    
    eff_prior_length=eff_prior_length+counter;
    
    
end
        
end


