function  [b_bar_new,map_delta] =  update_b_bar(Nrow,pixel,b_bar,b_new,b,alpha_GMRF)

    map_delta=0;
    b_bar_new=b_bar;
    n_col = ceil(pixel/Nrow);
    n_row = pixel-(n_col-1)*Nrow;
    Ncol=length(b_bar)/Nrow; 
    
    %% center
    mod_pix = pixel;
    b_bar_new(mod_pix)=b_bar_new(mod_pix)+(b_new-b)/5;
    map_delta=map_delta+log(b_bar_new(mod_pix))-log(b_bar(mod_pix));

    %% col to the right
    if n_col==Ncol
        mod_pix=n_row;
    else
        mod_pix=(n_col)*Nrow+n_row;
    end
    b_bar_new(mod_pix)=b_bar_new(mod_pix)+(b_new-b)/5;
    map_delta=map_delta+log(b_bar_new(mod_pix))-log(b_bar(mod_pix));
    
    %% col to the left
    if n_col==1 
        mod_pix=(Ncol-1)*Nrow+n_row;
    else
        mod_pix=(n_col-2)*Nrow+n_row;
    end
    b_bar_new(mod_pix)=b_bar_new(mod_pix)+(b_new-b)/5;
    map_delta=map_delta+log(b_bar_new(mod_pix))-log(b_bar(mod_pix));

    %% row down
    if n_row==Nrow 
        mod_pix=(n_col-1)*Nrow+1;
    else
        mod_pix=(n_col-1)*Nrow+n_row+1;
    end
    b_bar_new(mod_pix)=b_bar_new(mod_pix)+(b_new-b)/5;
    map_delta=map_delta+log(b_bar_new(mod_pix))-log(b_bar(mod_pix));

    %% row up
    if n_row==1
        mod_pix=(n_col-1)*Nrow+Nrow;
    else
        mod_pix=(n_col-1)*Nrow+n_row-1;
    end
    b_bar_new(mod_pix)=b_bar_new(mod_pix)+(b_new-b)/5;
    map_delta = map_delta+log(b_bar_new(mod_pix))-log(b_bar(mod_pix));
        
    %%
    map_delta=-alpha_GMRF*map_delta;
  end