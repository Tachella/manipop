

filename = 'image_40m_F100_02ms.mat';
load(filename)

if ~iscell(Y)
    
    if size(Y,3)==1
       Y = reshape(Y,sqrt(size(Y,1)),sqrt(size(Y,1)),size(Y,2)); 
    end

    Nrow=size(Y,1);

    factor=3;

    Nrow=Nrow-rem(Nrow,factor);
    
    Y = Y(1:Nrow,1:Nrow,:);
    [Y,Y_ind,T]=get_return_list(Y);


    save(filename,'Y','Y_ind','T')
end


