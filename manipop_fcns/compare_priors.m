close all
clear all
clc
%% preprocess step
lowpass_size=1;
 
%% Main algo
filename = 'shapes_synthetic.mat';
fig = 25;
bin_width = 1;
scaleA = 1;
plot_debug = false;


% test_filenames = {'out_shapes_synthetic_all.mat';'out_shapes_synthetic_nospatial.mat';'out_shapes_synthetic_noreflectivity.mat';'out_shapes_synthetic_onescale.mat';'out_shapes_synthetic_nodilate.mat';'out_shapes_synthetic_nobackground.mat'};

str = filename;
str = str(1:end-4);


%% No background reg.
fig=fig+1;
load(['data\' filename])
tic
%% coarse scale     
[Y,Y_ind,T,G_s,Nrow,scale_ratio] = downsample_poisson(Y,Y_ind,T,G,3,Nrow,scale_ratio);
[Nmc,lambda_area_int,log_gamma_area_int,alpha,alpha_GMRF,Nbin]=get_hyperparam(length(Y),0,scale_ratio);
alpha_GMRF = 1;
T0_prior = preprocess3D(Y,Y_ind,T,h,attack,decay,lowpass_size,Nrow,Nbin);
output = PointProcess_RJMCMC(Y,Y_ind,T,h,attack,decay,Nmc,T0_prior,[],[],[],log_gamma_area_int,lambda_area_int,G_s,alpha,alpha_GMRF,Nrow,scale_ratio,Nbin,plot_debug);

%% finer scale
[T0,A,B,T0_prior,Nrow,scale_ratio] = upsample_pointcloud(output{2},output{1},output{3},T0_prior,3,Nrow,scale_ratio,Nbin);
load(['data\' filename])
[Nmc,lambda_area_int,log_gamma_area_int,alpha,alpha_GMRF,Nbin]=get_hyperparam(length(Y),1,scale_ratio);
alpha_GMRF = 1;
output = PointProcess_RJMCMC(Y,Y_ind,T,h,attack,decay,Nmc,T0_prior,T0,A,B,log_gamma_area_int,lambda_area_int,G,alpha,alpha_GMRF,Nrow,scale_ratio,Nbin,plot_debug);
elapsed_time = toc;
save(['results\out_' str '_nobackground'],'output','Nrow','scale_ratio','elapsed_time') 
fig=plot_triang(output{2},output{1},scale_ratio,scale_ratio*5,fig,Nrow,bin_width,scaleA);



%% No multiscale
fig=fig+1;
%% finer scale
load(['data\' filename])
[Nmc,lambda_area_int,log_gamma_area_int,alpha,alpha_GMRF,Nbin]=get_hyperparam(length(Y),1,scale_ratio);
Nmc = Nmc*2;
output = PointProcess_RJMCMC(Y,Y_ind,T,h,attack,decay,Nmc,T0_prior,[],[],[],log_gamma_area_int,lambda_area_int,G,alpha,alpha_GMRF,Nrow,scale_ratio,Nbin,plot_debug);
elapsed_time = toc;
save(['results\out_' str '_onescale'],'output','Nrow','scale_ratio','elapsed_time') 
fig=plot_triang(output{2},output{1},scale_ratio,scale_ratio*5,fig,Nrow,bin_width,scaleA);




%% No reflectivity reg.
fig=fig+1;
load(['data\' filename])
tic
%% coarse scale     
[Y,Y_ind,T,G_s,Nrow,scale_ratio] = downsample_poisson(Y,Y_ind,T,G,3,Nrow,scale_ratio);
[Nmc,lambda_area_int,log_gamma_area_int,alpha,alpha_GMRF,Nbin]=get_hyperparam(length(Y),0,scale_ratio);
alpha = 100^2;
T0_prior = preprocess3D(Y,Y_ind,T,h,attack,decay,lowpass_size,Nrow,Nbin);
output = PointProcess_RJMCMC(Y,Y_ind,T,h,attack,decay,Nmc,T0_prior,[],[],[],log_gamma_area_int,lambda_area_int,G_s,alpha,alpha_GMRF,Nrow,scale_ratio,Nbin,plot_debug);

%% finer scale
[T0,A,B,T0_prior,Nrow,scale_ratio] = upsample_pointcloud(output{2},output{1},output{3},T0_prior,3,Nrow,scale_ratio,Nbin);
load(['data\' filename])
[Nmc,lambda_area_int,log_gamma_area_int,alpha,alpha_GMRF,Nbin]=get_hyperparam(length(Y),1,scale_ratio);
alpha = 100^2;
output = PointProcess_RJMCMC(Y,Y_ind,T,h,attack,decay,Nmc,T0_prior,T0,A,B,log_gamma_area_int,lambda_area_int,G,alpha,alpha_GMRF,Nrow,scale_ratio,Nbin,plot_debug);
elapsed_time = toc;
save(['results\out_' str '_noreflectivity'],'output','Nrow','scale_ratio','elapsed_time') 
fig=plot_triang(output{2},output{1},scale_ratio,scale_ratio*5,fig,Nrow,bin_width,scaleA);



%% No spatial reg.
fig=fig+1;
load(['data\' filename])
tic
%% coarse scale     
[Y,Y_ind,T,G_s,Nrow,scale_ratio] = downsample_poisson(Y,Y_ind,T,G,3,Nrow,scale_ratio);
[Nmc,lambda_area_int,log_gamma_area_int,alpha,alpha_GMRF,Nbin]=get_hyperparam(length(Y),0,scale_ratio);
log_gamma_area_int = 0;
T0_prior = preprocess3D(Y,Y_ind,T,h,attack,decay,lowpass_size,Nrow,Nbin);
output = PointProcess_RJMCMC(Y,Y_ind,T,h,attack,decay,Nmc,T0_prior,[],[],[],log_gamma_area_int,lambda_area_int,G_s,alpha,alpha_GMRF,Nrow,scale_ratio,Nbin,plot_debug);

%% finer scale
[T0,A,B,T0_prior,Nrow,scale_ratio] = upsample_pointcloud(output{2},output{1},output{3},T0_prior,3,Nrow,scale_ratio,Nbin);
load(['data\' filename])
[Nmc,lambda_area_int,log_gamma_area_int,alpha,alpha_GMRF,Nbin]=get_hyperparam(length(Y),1,scale_ratio);
log_gamma_area_int = 0;
output = PointProcess_RJMCMC(Y,Y_ind,T,h,attack,decay,Nmc,T0_prior,T0,A,B,log_gamma_area_int,lambda_area_int,G,alpha,alpha_GMRF,Nrow,scale_ratio,Nbin,plot_debug);
elapsed_time = toc;
save(['results\out_' str '_nospatial'],'output','Nrow','scale_ratio','elapsed_time') 
fig=plot_triang(output{2},output{1},scale_ratio,scale_ratio*5,fig,Nrow,bin_width,scaleA);








