function [A,map_delta]=mark_move(Y,Y_ind,h,attack,decay,A,T0,B,integrated_h,PPP,G,Nbin,alpha,scale_Z,Nrow,ppp)
    
%%
    map_delta = 0;
    sigma_RWM = 0.5;
    
    u=rand*sum(ppp.*(1:length(ppp))');
    list=0; csum=0;
    while(u>csum)
        list=list+1;
        csum=csum+ppp(list)*list;
    end
    
    list_pixel=randi(ppp(list));
    p=PPP{list};
    pixel=p(list_pixel);
    

    t0=T0{pixel};  a=A{pixel};
    b=B(pixel); g = G(pixel);
    
    index=randi(length(a));
    
    
    [prec, mean_a]= get_GP_par(t0(index), pixel, A, T0, alpha, scale_Z, Nrow, Nbin, true);
    
    %% current log probability
    curr = compute_likelihood(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay);
    
    prior_curr=-1/2*(a(index)-mean_a)^2*prec;
    
    
    %% proposal log_prob
    a_prop=a;
    a_prop(index) = a(index)+randn*sigma_RWM;
    
    prop = compute_likelihood(Y,Y_ind,G,pixel,b,t0,a_prop,h,attack,decay);
    prior_prop = -1/2*(a_prop(index)-mean_a)^2*prec;
    
    
    %% accept-reject
    likelihood=prop-curr-(exp(a_prop(index))-exp(a(index)))*g*integrated_h;
    if exp(likelihood + prior_prop - prior_curr)>rand
        %% save A
        A{pixel}=a_prop(:);
        %% map delta
        map_delta = prior_prop-prior_curr+likelihood;
        
    end
    %plot(y)
    %hold on
    %plot(x)
    %plot(x_curr)
    
end