function [T0,A,B,PPP,eff_prior_length]=init_all(T0,A,B,max_points_per_pixel,T0_prior,eff_prior_length,min_dist,N,integrated_h)


PPP=cell(max_points_per_pixel,1);
%% init A, B and T0, PPP and make sure prior is respected
if isempty(B) || isempty(T0) || isempty(A)
    T0 = cell(N,1);
    A = cell(N,1);
    B = rand(N,1)*1e-3;
else
    sh = log(integrated_h);
    for n=1:N
        t0=T0{n};
        A{n} = log(A{n})-sh;
        for j=1:length(t0)
            if sum((t0(j)-T0_prior{n})==0)==0
                T0{n}=[];
                A{n}=[];
                break
            else
                eff_prior_length = modify_prior(0,T0_prior,n,t0(j),min_dist,eff_prior_length);
            end
        end
        if ~isempty(t0)
            ppp=PPP{length(t0)};
            ppp(end+1)=n;
            PPP{length(t0)}=ppp(:);
        end
    end
end

end