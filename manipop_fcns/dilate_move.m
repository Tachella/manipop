function [A,T0,B,b_bar,map_delta,mdB,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points]=dilate_move(Y,Y_ind,T,h,attack,decay,A,T0,B,b_bar,alpha_GMRF,prior_pixels,lambda_S,gamma_strauss,max_dist,occupied_volume,prior_length,PPP,total_points,integrated_h,Npix,Nbin,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,log_gamma_area_int,lambda_area_int,G,alpha,scale_Z,K,Nrow,Mergeable_pixels)
    
    mdB=0;
    map_delta=0;
    N=length(Y);
    Ncol=N/Nrow;
    %% pick a pixel from existing ones
    accepted=false;
    s_PPP=sum(sum_PPP.*(1:length(sum_PPP))');
    while ~accepted
        u=rand*s_PPP;
        list=0; csum=0;
        while(u>csum)
            list=list+1;
            csum=csum+sum_PPP(list)*list;
        end
        list_pixel=randi(sum_PPP(list));
        p=PPP{list};
        center_pixel=p(list_pixel);
        index=randi(list);
        neigh=NEIGH{center_pixel};
        bord = is_border(center_pixel,Nrow,Ncol);
        if neigh(index) < 8 - (bord>0) -2*bord
             accepted=true;
        end
    end
    
    
    %% pick a neighboring pixel
    n_col = ceil(center_pixel/Nrow);
    n_row = center_pixel-(n_col-1)*Nrow;
    %%pick a bin
    t0_center_pix = T0{center_pixel};
    t0_center_pix=t0_center_pix(index);
    
    t0_new=randi([t0_center_pix-Nbin,t0_center_pix+Nbin]);
    
    order=randperm(8);
    strauss=Inf; p=1;
    while isinf(strauss) %% 
        pixel=select_seq_neighbor(Nrow,Ncol,n_row,n_col,order(p));
        if pixel
            t0 = T0{pixel};
            strauss=0;
            for j=1:length(t0)
                if abs(t0_center_pix-t0(j))<=Nbin
                    strauss=Inf;
                    break
                end
            end
        end
        p=p+1;
    end
    
    
    %% strauss term
    strauss=0;
    for j=1:length(t0)
        if abs(t0_new-t0(j))<=max_dist
            strauss=strauss+log(gamma_strauss);
        end
    end
    
    if ~isinf(strauss) && sum((t0_new-prior_pixels{pixel})==0)
       
        %% compute resulting neighbors
%         [neigh_sum_prime,new_neigh] = neighborhood_change(occupied_volume,pixel,t0_new,Nbin,NEIGH,T0,neigh_sum);
        
        a=A{pixel};
        b=B(pixel); 
      
        %% propose a peak
        
        [prec, mean_a, det_term] = get_GP_par(t0_new,pixel,A,T0,alpha,scale_Z,Nrow,Nbin,false);

        sigma = 1/sqrt(prec);
        a_new = mean_a + randn*sigma;
        
        
        b_new = b-exp(a_new)*integrated_h/T;
        
        if b_new>0 

            a_prop=a;
            t0_prop=t0;

            a_prop(end+1,1)=a_new;
            t0_prop(end+1,1)=t0_new;

            %% proposal logprobability
            prop = compute_likelihood(Y,Y_ind,G,pixel,b_new,t0_prop,a_prop,h,attack,decay);

            prop = prop-1/2*prec*(mean_a-a_new)^2 + det_term;
            prop_prior = (alpha_GMRF-1) * log(b_new);


            %% current logprobability
            curr = compute_likelihood(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay);

            curr_prior = (alpha_GMRF-1)*log(b);

            %% other GMRF term
            [b_bar_new,map_GMRF] = update_b_bar(Nrow,pixel,b_bar,b_new,b,alpha_GMRF);

            %% non sym dilation term
            n_col = ceil(pixel/Nrow);
            n_row = pixel-(n_col-1)*Nrow;
            total_neigh = occupied_volume(n_row,n_col,t0_new);
            term = -log(total_points/total_neigh)-log(2*Nbin+1);

            %% Poisson ref measure 
            ref_measure = log(lambda_S)-log(prior_length);
            
            %% non symmetrical proposal term
            nonsym= - term   - log(points_with_neigh+1) - 1/2*(log(prec)-log(2*pi)-prec*(a_new-mean_a)^2);

            %% area interaction
            log_penalty = area_interaction(0,occupied_volume,pixel,t0_new,[],[],Npix,Nbin,log_gamma_area_int,lambda_area_int);

            %% accept/reject
            if rand<exp(prop+prop_prior-curr-curr_prior+strauss+log_penalty+map_GMRF+ref_measure+nonsym)
                %% save new estimates
                A{pixel}=a_prop;
                T0{pixel}=t0_prop;
                b_bar=b_bar_new;
                

                total_points = total_points+1;
                Mergeable_pixels = update_mergeable_list(t0_prop,t0,attack+decay,pixel,Mergeable_pixels);
            
                
                if ~isempty(a) %remove pixel from list of length(a) points
                   p=PPP{length(a)};
                   p(p==pixel)=[];
                   PPP{length(a)}=p;
                   sum_PPP(length(a))=sum_PPP(length(a))-1;
                end

                PPP{length(a_prop)}=[PPP{length(a_prop)};pixel];
                sum_PPP(length(a_prop))=sum_PPP(length(a_prop))+1;

                B(pixel) = b_new;

                %% compute map
                map_delta=ref_measure+prop-curr+strauss+log_penalty+prop_prior-curr_prior+map_GMRF;

                mdB=prop_prior-curr_prior+map_GMRF;
                [occupied_volume,NEIGH,points_with_neigh,neigh_sum]=modify_volume(0,occupied_volume,pixel,t0_new,[],Npix,Nbin,NEIGH,T0,[],points_with_neigh,neigh_sum);
                eff_prior_length=modify_prior(0,prior_pixels,pixel,t0_new,max_dist,eff_prior_length);
            
                K(pixel,length(a_prop)+1)=K(pixel,length(a_prop)+1)+1;
            else
                K(pixel,length(a)+1)=K(pixel,length(a)+1)+1;
            end
        end
    end
    
end