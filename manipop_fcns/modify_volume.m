function [occupied_volume,NEIGH,points_with_neigh,neigh_sum]=modify_volume(type,occupied_volume,n,t0_new,t0_death,Npix2,Nbin2,NEIGH,T0,death_index,points_with_neigh,neigh_sum)

% type: indicator of RJ move
% 0 -> birth
% 1 -> death
% 2 -> shift
[Nrow,Ncol,~]=size(occupied_volume);

n_col = ceil(n/Nrow);
n_row = n-(n_col-1)*Nrow;

neigh=NEIGH{n};

if type==0 % birth
    
    s=occupied_volume(n_row,n_col,t0_new);
    if s>0
       neigh(end+1) = s;
       neigh_sum(s) = neigh_sum(s) + 1;
       NEIGH{n}=neigh(:);
       points_with_neigh=points_with_neigh+1;
       
       %update neighbors
        for iter=1:8
           n_prime=select_seq_neighbor(Nrow,Ncol,n_row,n_col,iter);
           if n_prime
               t0=T0{n_prime};
               for bin=1:length(t0)
                   if abs(t0(bin)-t0_new) <= Nbin2
                       neigh=NEIGH{n_prime};
                       if neigh(bin)==0
                           points_with_neigh=points_with_neigh+1;
                       else
                            neigh_sum(neigh(bin)) = neigh_sum(neigh(bin))-1;
                       end
                       neigh(bin) = neigh(bin)+1;
                       neigh_sum(neigh(bin)) = neigh_sum(neigh(bin))+1;
                       NEIGH{n_prime}=neigh(:);
                       break;
                   end
               end
           end
        end
    else
       neigh(end+1) = 0;
       NEIGH{n}=neigh(:);
    end
    
    b=max([n_col-Npix2,1]):min([n_col+Npix2,Ncol]);
    a=max([n_row-Npix2,1]):min([n_row+Npix2,Nrow]);
    c=t0_new-Nbin2:t0_new+Nbin2;
    occupied_volume(a,b,c)=occupied_volume(a,b,c)+1;   

elseif type==1 % death
    
   s=occupied_volume(n_row,n_col,t0_death); 
   neigh(death_index) = [];
   NEIGH{n}=neigh(:);
   if s>1
       neigh_sum(s-1) = neigh_sum(s-1) - 1;
       points_with_neigh=points_with_neigh-1;
       %update neighbors
       for iter=1:8
           n_prime=select_seq_neighbor(Nrow,Ncol,n_row,n_col,iter);
           if n_prime
               t0=T0{n_prime};
               for bin=1:length(t0)
                   if abs(t0(bin)-t0_death) <= Nbin2
                       neigh = NEIGH{n_prime};
                       neigh_sum(neigh(bin)) = neigh_sum(neigh(bin))-1;
                       neigh(bin) = neigh(bin)-1;
                       if neigh(bin) == 0
                           points_with_neigh = points_with_neigh-1;
                       else
                           neigh_sum(neigh(bin)) = neigh_sum(neigh(bin))+1; 
                       end
                       NEIGH{n_prime}=neigh(:);
                       break;
                   end
               end
           end
       end
    end
    
    b=max([n_col-Npix2,1]):min([n_col+Npix2,Ncol]);
    a=max([n_row-Npix2,1]):min([n_row+Npix2,Nrow]);
    c=t0_death-Nbin2:t0_death+Nbin2 ;
    occupied_volume(a,b,c)=occupied_volume(a,b,c)-1; 
    
else % shift

    b=max([n_col-Npix2,1]):min([n_col+Npix2,Ncol]);
    a=max([n_row-Npix2,1]):min([n_row+Npix2,Nrow]);
    c=t0_death-Nbin2:t0_death+Nbin2;
    occupied_volume(a,b,c)=occupied_volume(a,b,c)-1;
    s1=occupied_volume(n_row,n_col,t0_death); 
   
    
    c=t0_new-Nbin2:t0_new+Nbin2;
    
    s2=occupied_volume(n_row,n_col,t0_new); 
    
    occupied_volume(a,b,c)=occupied_volume(a,b,c)+1;
    
    if s1>0 || s2>0
        neigh(death_index)=s2;
        if s1==0
            points_with_neigh = points_with_neigh+1;
            neigh_sum(s2) = neigh_sum(s2)+1;
        elseif s2==0
            points_with_neigh = points_with_neigh-1;
            neigh_sum(s1) = neigh_sum(s1)-1;
        else
            neigh_sum(s1) = neigh_sum(s1)-1;
            neigh_sum(s2) = neigh_sum(s2)+1;
        end
        NEIGH{n} = neigh(:);
        
       %update neighbors
       for iter=1:8
           n_prime=select_seq_neighbor(Nrow,Ncol,n_row,n_col,iter);
           if n_prime
               t0=T0{n_prime};
               for bin=1:length(t0)
                   if abs(t0(bin)-t0_new)<=Nbin2 && abs(t0(bin)-t0_death)>Nbin2
                       neigh=NEIGH{n_prime};
                       if neigh(bin)==0
                           points_with_neigh=points_with_neigh+1;
                       else
                           neigh_sum(neigh(bin)) = neigh_sum(neigh(bin))-1;
                       end
                       neigh(bin) = neigh(bin)+1;
                       neigh_sum(neigh(bin)) = neigh_sum(neigh(bin))+1;
                       NEIGH{n_prime}=neigh(:);
                   elseif abs(t0(bin)-t0_new)>Nbin2 && abs(t0(bin)-t0_death)<=Nbin2
                       neigh = NEIGH{n_prime};
                       neigh_sum(neigh(bin)) = neigh_sum(neigh(bin))-1;
                       neigh(bin) = neigh(bin)-1;
                       if neigh(bin) == 0
                           points_with_neigh=points_with_neigh-1;
                       else
                           neigh_sum(neigh(bin)) = neigh_sum(neigh(bin))+1; 
                       end
                       NEIGH{n_prime}=neigh(:);
                   end
               end
           end
       end
       
    end
   % end
end



    
