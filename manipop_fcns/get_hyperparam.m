function [Nmc,lambda_area_int,log_gamma_area_int,alpha,alpha_GMRF,Nbin] = get_hyperparam(length_Y,scale,scale_ratio)

alpha = (.6)^2; %it is for the log-image!
alpha_GMRF = 2;

if scale == 0
    Nmc = length_Y*25*9;
    log_gamma_area_int = 3;
    lambda_area_int = log(length_Y)*1.2;
    Nbin = round(4*scale_ratio/3);
else
    Nmc = length_Y*25;
    log_gamma_area_int = 3;
    lambda_area_int = log(length_Y)*1.4;
    alpha = alpha/3;
    Nbin = round(4*scale_ratio);
end

end