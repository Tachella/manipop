clear all; close all; clc;
%% READ ME FIRST
% Paper: Bayesian 3D Reconstruction of Complex Scenes from Single-Photon Lidar Data
% Author List: J. Tachella,	Y. Altmann,	X. Ren, A. McCarthy, G. S. Buller,
% J.-Y. Tourneret, 	and S. McLaughlin
% Contact: J. Tachella, email: jat3@hw.ac.uk
%% DATA INPUT
% A Lidar dataset (of size Nr x Nc x T) is saved in a .mat file in the data folder and must
% have the following variables:
% Y (type: cell) size(Y)= [Nr,Nc,T] : Lidar cube of Nr by Nc pixels and T
% histogram bins
% h (type: double) size(h) = [length of impulse response, 1] : vector
% containing impulse response
% G (type: double) size(G) = [Nr,Nc] : pixel wise gain of the Lidar 
% device, between [0,1]. It can also be used to model any compressive
% sensing strategy. By default set to G = ones(Nr,Nc);
% bin_width (type: double) size(bin_width)=1 : bin width in millimetres, it
% can be obtained as speed_of_light/2*TCSPC_binning
% scale_ratio (type: double) size(scale_ratio)=1 : approximate ratio
% between the width of a pixel and the width of a bin, computed as
% pixel_width/bin_width

%% OUTPUT
% The reconstructed 3D point cloud is saved in the folder \results with
% .ply formatting.
% Additionally, a .mat file is saved with the following variables:
% p (type: pointCloud) : Point Cloud with points in real-world coordinates
% (same as .ply file)
% intensity_im (type: double) size(intensity_im) = Nr x Nc, intensity image
% keeps only one point per pixel (the one with highest intensity)
% depth_im (type: double) size(intensity_im) = Nr x Nc, depth image
% keeps only one point per pixel (the one with highest intensity)
% elapsed_time (type: double) size(elapsed_time) = 1, contains execution
% time in seconds

%% Acknowledgments
% The mit_mannequin was taken from Dongeek Shin, Feihu Xu, Franco N. C. Wong, Jeffrey H. Shapiro, and Vivek K Goyal, "Computational multi-depth single-photon imaging," Opt. Express 24, 1873-1888 (2016)

%% Examples: just uncomment the desired dataset and run this script
%filename = 'mit_mannequin';
filename = 'hw_head_40m_1ms.mat';
%filename = 'image_325m_F500_R50_1219hrs_3ms.mat';
%filename = 'truncated_hw_head_40m_1ms.mat';
%filename = 'synthetic_art_scene.mat';

addpath('manipop_fcns')
run_dataset(filename)
